package main.kotlin.com.antzucaro.aoc.day6

import java.io.File
import kotlin.collections.ArrayDeque


fun main(args: Array<String>) {
    File(args[0]).forEachLine { line ->
        println(firstUniqueIn(line, 14))
    }
}
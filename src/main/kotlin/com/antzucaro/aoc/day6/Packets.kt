package main.kotlin.com.antzucaro.aoc.day6

import java.io.File
import kotlin.collections.ArrayDeque

fun firstUniqueIn(line: String, chunkSize: Int): Int {
    val q = ArrayDeque<Char>()

    for ((i, c) in line.toCharArray().withIndex()) {
        if(q.size == chunkSize) {
            // Packed queue already, create space
            q.removeFirst()
        }
        q.add(c)

        // Check unique
        if (q.toSet().size == chunkSize) {
            return i + 1
        }
    }

    return -1
}

fun main(args: Array<String>) {
    File(args[0]).forEachLine { line ->
        println(firstUniqueIn(line, 4))
    }
}
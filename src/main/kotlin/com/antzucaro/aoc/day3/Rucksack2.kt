package main.kotlin.com.antzucaro.aoc.day3

import java.io.File

fun score(c: Char): Int {
    val ordinal = c.toInt()
    return if (ordinal >= 'a'.toInt()) {
        ordinal - 'a'.toInt() + 1
    } else {
        ordinal - 'A'.toInt() + 27
    }
}

fun main(args: Array<String>) {
    var i = 1
    var contents = ArrayList<String>();
    var sum = 0

    File(args[0]).forEachLine {
        contents.add(it)
        if (i % 3 == 0) {
            val common = contents[0].toSet().intersect(contents[1].toSet()).intersect(contents[2].toSet())
            sum += score(common.first())

            // Reset
            i = 1
            contents = ArrayList<String>();
        } else {
            i++
        }
    }

    println(sum)
}
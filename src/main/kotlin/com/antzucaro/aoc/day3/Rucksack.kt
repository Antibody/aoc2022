package main.kotlin.com.antzucaro.aoc.day3

import java.io.File

class Rucksack(items: String) {
    val compartmentOne = items.substring(0, items.length/2)
    val compartmentTwo = items.substring(items.length/2, items.length)

    fun common(): Char {
        val commonChars = compartmentOne.toSet().intersect(compartmentTwo.toSet())
        return commonChars.first()
    }

    fun score(c: Char): Int {
        val ordinal = c.toInt()
        return if (ordinal >= 'a'.toInt()) {
            ordinal - 'a'.toInt() + 1
        } else {
            ordinal - 'A'.toInt() + 27
        }
    }
}

fun main(args: Array<String>) {
    val rucksacks = ArrayList<Rucksack>();
    File(args[0]).forEachLine {
        rucksacks.add(Rucksack(it))
    }

    var sum = 0
    for(rucksack in rucksacks) {
        println("${rucksack.compartmentOne} ${rucksack.compartmentTwo} ${rucksack.common()} ${rucksack.score(rucksack.common())}")
        sum += rucksack.score(rucksack.common())
    }
    println(sum)
}
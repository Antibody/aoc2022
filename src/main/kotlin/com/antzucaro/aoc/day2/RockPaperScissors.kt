package main.kotlin.com.antzucaro.aoc.day2

import java.io.File

const val LOSS_SCORE = 0
const val DRAW_SCORE = 3
const val WIN_SCORE = 6

data class Move(val name: String, val score: Int): Comparable<Move> {
    override fun compareTo(other: Move): Int {
        return when {
            this.name == other.name -> { 0 }
            this.name == "Rock" && other.name == "Scissors" -> { 1 }
            this.name == "Rock" && other.name == "Paper" -> { -1 }
            this.name == "Paper" && other.name == "Rock" -> { 1 }
            this.name == "Paper" && other.name == "Scissors" -> { -1 }
            this.name == "Scissors" && other.name == "Paper" -> { 1 }
            this.name == "Scissors" && other.name == "Rock" -> { -1 }
            else -> { 0 }
        }
    }
}

val Rock = Move("Rock", 1)
val Paper = Move("Paper", 2)
val Scissors = Move("Scissors", 3)

fun decode(key: String): Move {
    return when (key) {
        "A" -> { Rock }
        "B" -> { Paper }
        "C" -> { Scissors }
        else -> { throw Exception("invalid key $key")}
    }
}

fun losesAgainst(move: Move): Move {
    return when (move) {
        Rock -> { Scissors }
        Paper -> { Rock }
        Scissors -> { Paper }
        else -> { throw Exception("invalid move $move")}
    }
}

fun drawsAgainst(move: Move): Move {
    return move;
}

fun winsAgainst(move: Move): Move {
    return when (move) {
        Rock -> { Paper }
        Paper -> { Scissors }
        Scissors -> { Rock }
        else -> { throw Exception("invalid move $move")}
    }
}

fun decide(move: Move, key: String): Move {
    return when (key) {
        "X" -> { losesAgainst(move) }
        "Y" -> { drawsAgainst(move) }
        "Z" -> { winsAgainst(move) }
        else -> { throw Exception("invalid key $key")}
    }
}

class RockPaperScissorsPlayer {

    fun process(filename: String) {
        println("Processing $filename");

        var score = 0;
        File(filename).forEachLine { line ->
            println(line)
            val tokens = line.split(" ")
            val opponentMove = decode(tokens[0])
            val myMove = decide(opponentMove, tokens[1])

            println("${opponentMove.name} ${myMove.name}")

            if (myMove < opponentMove) {
                // Loss
                score += myMove.score + LOSS_SCORE
                println("result: loss")
            } else if (myMove == opponentMove) {
                // Draw
                score += myMove.score + DRAW_SCORE
                println("result: draw")
            } else {
                // Win
                score += myMove.score + WIN_SCORE
                println("result: win")
            }
        }
        println(score)
    }
}

fun main(args : Array<String>) {
    val player = RockPaperScissorsPlayer()
    player.process(args[0])
}

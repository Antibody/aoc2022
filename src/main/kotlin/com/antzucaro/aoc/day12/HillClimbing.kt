package main.kotlin.com.antzucaro.aoc.day12

import main.kotlin.com.antzucaro.aoc.day9.Pos
import java.io.File

class HillClimbing(var grid: List<List<Char>>) {
    private var S: Pos = find('S')
    private var E: Pos = find('E')

    private fun find(c: Char): Pos {
        for (row in grid.indices) {
            for (col in grid[row].indices) {
                if (grid[row][col] == c) {
                    return Pos(row, col)
                }
            }
        }

        // Should never occur, but...
        return Pos(0, 0)
    }
}

fun load(file: String): List<List<Char>> {
    val content = File(file).useLines { it.toList() }
    return content.map { row -> row.toCharArray().toList() }
}

fun main(args: Array<String>) {
    val grid = load(args[0])
}
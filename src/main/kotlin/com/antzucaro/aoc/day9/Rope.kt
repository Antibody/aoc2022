package main.kotlin.com.antzucaro.aoc.day9

import java.io.File
import kotlin.math.pow
import kotlin.math.round
import kotlin.math.sign
import kotlin.math.sqrt

data class Pos(var x: Int, var y: Int) {
    override fun toString(): String {
        return "($x, $y)"
    }

    fun distanceTo(other: Pos): Double {
        return round(sqrt((other.x - this.x).toDouble().pow(2.0) + (other.y - this.y).toDouble().pow(2.0)))
    }
}

class Rope(numKnots: Int) {
    private var knots = Array(numKnots) { Pos(0, 0) }

    // Keep track of was positions the tail has seen
    private var uniques = HashSet<String>()
    init {
        uniques.add(knots[0].toString())
    }

    private fun adjust() {
        for (i in 0 until knots.lastIndex) {
            if(knots[i+1].distanceTo(knots[i]) > 1.0) {
                val diffX = knots[i].x - knots[i+1].x
                val diffY = knots[i].y - knots[i+1].y

                knots[i+1].x += sign(diffX.toDouble()).toInt()
                knots[i+1].y += sign(diffY.toDouble()).toInt()
            }
        }

        // Keep track of only where the last knot (the tail) touches.
        uniques.add(knots[knots.lastIndex].toString())
    }

    fun up() {
        knots[0].y++
        adjust()
    }

    fun down() {
        // Move down by the specified amount
        knots[0].y--
        adjust()
    }

    fun left() {
        knots[0].x--
        adjust()
    }

    fun right() {
        knots[0].x++
        adjust()
    }

    fun numUniques(): Int {
        return uniques.size
    }
}

fun load(file: String, numKnots: Int): Rope {

    var rope = Rope(numKnots)
    File(file).forEachLine { line ->
        val (direction, amountStr) = line.split(" ")
        val amount = Integer.parseInt(amountStr)

        println(line)
        for(i in 0 until amount) {
            when (direction) {
                "U" -> rope.up()
                "D" -> rope.down()
                "L" -> rope.left()
                "R" -> rope.right()
            }
        }
    }

    return rope
}

fun main(args: Array<String>) {
    // Part 1
    val ropePart1 = load(args[0], 2)
    println("number of unique locations visited in part 1: ${ropePart1.numUniques()}")

    // Part 2
    val ropePart2 = load(args[0], 10)
    println("number of unique locations visited in part 2: ${ropePart2.numUniques()}")
}
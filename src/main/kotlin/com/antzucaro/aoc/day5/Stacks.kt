package main.kotlin.com.antzucaro.aoc.day5

import java.io.File
import kotlin.collections.ArrayDeque
import kotlin.collections.ArrayList

fun main(args: Array<String>) {
    var queues: ArrayList<ArrayDeque<String>> = ArrayList();

    File(args[0]).forEachLine {
        if (it.trimStart().startsWith('[')) {
            // Stack piece line
            for((i, start) in (0..it.length).step(4).withIndex()) {
                // Grow stacks if needed
                while(queues.size < i+1) {
                    queues.add(ArrayDeque())
                }

                val piece = it.substring(start+1, start+2)
                if (piece.isNotBlank()) {
                    queues[i].addLast(piece)
                }
            }
        }

        if (it.trimStart().startsWith("move")) {
            // Move line
            val match = Regex("move (\\d+) from (\\d+) to (\\d+)").find(it)!!
            val (countStr, sourceStr, targetStr) = match.destructured

            val count = Integer.parseInt(countStr)
            val source = Integer.parseInt(sourceStr)
            val target = Integer.parseInt(targetStr)

            for (i in 1..count) {
                queues[target-1].addFirst(queues[source-1].removeFirst())
            }
        }
    }

    for (q in queues) {
        print(q.first())
    }
}
package main.kotlin.com.antzucaro.aoc.day8

import java.io.File

class TreeGrid (private val trees: ArrayList<List<Int>>){
    private val rows = trees.size
    private val columns = trees[0].size

    private fun numEdges(): Int {
        return (2 * rows) + (2 * columns) - 4
    }

    private fun isEdge(row: Int, column: Int): Boolean {
        if (row == 0 || column == 0) {
            // First row or first column
            return true
        }

        if (row == (rows - 1) || column == (columns - 1)) {
            // Last row or last column
            return true
        }

        return false
    }

    private fun visibleUp(row: Int, column: Int): Boolean {
        for(r in (row-1) downTo 0) {
            if (trees[r][column] >= trees[row][column]) {
                return false
            }
        }
        return true
    }

    private fun visibleDown(row: Int, column: Int): Boolean {
        for(r in (row+1) until rows) {
            if (trees[r][column] >= trees[row][column]) {
                return false
            }
        }
        return true
    }

    private fun visibleLeft(row: Int, column: Int): Boolean {
        for(c in (column-1) downTo 0) {
            if (trees[row][c] >= trees[row][column]) {
                return false
            }
        }
        return true
    }

    private fun visibleRight(row: Int, column: Int): Boolean {
        for(c in (column+1) until columns) {
            if (trees[row][c] >= trees[row][column]) {
                return false
            }
        }
        return true
    }

    private fun isVisible(row: Int, column: Int): Boolean {

        if (isEdge(row, column)) {
            return true
        }

        return visibleUp(row, column) || visibleDown(row, column) ||
                visibleLeft(row, column) || visibleRight(row, column)
    }

    fun visibleFromOutside(): Int {
        var numVisible = 0
        for(row in 1..rows-2) {
            for (column in 1..columns-2) {
               if (isVisible(row, column)) {
                   numVisible ++
               }
            }
        }
        return numEdges() + numVisible
    }

    private fun scenicUp(row: Int, column: Int): Int {
        var scenic = 0
        for(r in (row-1) downTo 0) {
            scenic++
            if (trees[r][column] >= trees[row][column]) {
                break
            }
        }
        return scenic
    }

    private fun scenicDown(row: Int, column: Int): Int {
        var scenic = 0
        for(r in (row+1) until rows) {
            scenic++
            if (trees[r][column] >= trees[row][column]) {
                break
            }
        }
        return scenic
    }

    private fun scenicLeft(row: Int, column: Int): Int {
        var scenic = 0
        for(c in (column-1) downTo 0) {
            scenic++
            if (trees[row][c] >= trees[row][column]) {
                break
            }
        }
        return scenic
    }

    private fun scenicRight(row: Int, column: Int): Int {
        var scenic = 0
        for(c in (column+1) until columns) {
            scenic++
            if (trees[row][c] >= trees[row][column]) {
                break
            }
        }
        return scenic
    }

    fun scenicScore(row: Int, column: Int): Int {
        val up = scenicUp(row, column)
        val down = scenicDown(row, column)
        val left = scenicLeft(row, column)
        val right = scenicRight(row, column)

        val elem = trees[row][column]
        val score = up * down * left * right

        println("$row, $column ($elem) -> $score ($up $left $down $right)")

        return score
    }

   fun maxScenicScore(): Int {
        var maxScore = 0
        for(row in 1..rows-2) {
            for (column in 1..columns-2) {
                val score = scenicScore(row, column)
                if (score > maxScore) {
                    maxScore = score
                }
            }
        }
        return maxScore
    }
}

fun load(file: String): TreeGrid {
    val trees = ArrayList<List<Int>>()
    File(file).forEachLine { line ->
        trees.add(line.toCharArray().map { c -> Integer.parseInt(c.toString()) })
    }

    return TreeGrid(trees)
}
package main.kotlin.com.antzucaro.aoc.day8

class Part2 {
}

fun main(args: Array<String>) {
    val grid = load(args[0])
    println(grid.maxScenicScore())
}

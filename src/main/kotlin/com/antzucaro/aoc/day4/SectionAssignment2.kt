package main.kotlin.com.antzucaro.aoc.day4

import java.io.File

fun main(args: Array<String>) {
    var numOverlaps = 0
    File(args[0]).forEachLine {
        val (firstSection, secondSection) = it.split(",")
        val assignment1 = sectionToSectionAssignment(firstSection)
        val assignment2 = sectionToSectionAssignment(secondSection)

        if (assignment1.overlaps(assignment2)) {
            numOverlaps++
        }
    }

    println(numOverlaps)
}
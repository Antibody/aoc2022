package main.kotlin.com.antzucaro.aoc.day4

import java.io.File

class SectionAssignment(private val start: Int, private val end: Int) {
    private val range = IntRange(start, end);

    fun contains(other: SectionAssignment): Boolean {
        return this.range.first >= other.range.first && this.range.last <= other.range.last
    }

    fun overlaps(other: SectionAssignment): Boolean {
        val elems = this.range.intersect(other.range)
        return elems.isNotEmpty()
    }
}

fun sectionToSectionAssignment(section: String): SectionAssignment {
    val (start, end) = section.split("-")
    return SectionAssignment(Integer.parseInt(start), Integer.parseInt(end))
}

fun main(args: Array<String>) {
    var numContains = 0
    File(args[0]).forEachLine {
        val (firstSection, secondSection) = it.split(",")
        val assignment1 = sectionToSectionAssignment(firstSection)
        val assignment2 = sectionToSectionAssignment(secondSection)

        if (assignment1.contains(assignment2) || assignment2.contains(assignment1)) {
            numContains++
        }
    }

    println(numContains)
}
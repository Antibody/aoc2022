package main.kotlin.com.antzucaro.aoc.day7

import java.io.File

const val CD_PREFIX = "$ cd "
const val LS_PREFIX = "$ ls"
const val DIR_PREFIX = "dir "

enum class INodeType {
    DIR, FILE
}

class Filesystem(val root: INode) {
    var head = root

    fun cd(name: String) {
        if (name == "/" || name == "") {
            head = root
        }

        for (node in head.children) {
            if(node.name == name) {
                head = node
                break
            }
        }
    }

    fun size(): Int {
        return head.totalSize()
    }
}

class INode(val name: String, val nodeType: INodeType, var size: Int = 0, val parent: INode? = null){
    public val children = ArrayList<INode>()

    fun addChild(child: INode) {
        children.add(child)
    }

    fun totalSize(): Int {
        var sum = size
        for (child in children) {
            sum += child.totalSize()
        }

        return sum
    }

    override fun toString(): String {
        return "INode(name='$name', nodeType=$nodeType, size=${totalSize()})"
    }

}

fun load(file: String): Filesystem {
    var fs = Filesystem(INode("/", INodeType.DIR))
    File(file).forEachLine { line ->
        if (line.startsWith(CD_PREFIX)) {
            val d  = line.substring(CD_PREFIX.length, line.length).trimEnd()

            // Initialze Filesystem with first directory we traverse
            if (d == "/") {
                // We already added upon init
            } else if (d == "..") {
                // Move up one directory.
                fs.head = fs.head.parent!!
            } else {
                // Change directory to that child
                fs.cd(d)
            }
        } else if (line.startsWith(LS_PREFIX)) {
            // Do nothing?
        } else if (line.startsWith(DIR_PREFIX)) {
            // New directory added to head
            val d  = line.substring(DIR_PREFIX.length, line.length).trimEnd()
            fs.head.addChild(INode(d, INodeType.DIR, 0, fs.head))
        } else {
            // File listing - add to head
            val (sizeStr, name) = line.split(" ")
            val size = Integer.parseInt(sizeStr)
            fs.head.addChild(INode(name, INodeType.FILE, size, fs.head))
        }
    }

    return fs
}
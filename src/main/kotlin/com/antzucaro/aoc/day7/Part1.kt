package main.kotlin.com.antzucaro.aoc.day7

import java.util.*
import kotlin.collections.HashSet

class Part1 {
}

fun main(args: Array<String>) {
    val fs = load(args[0])

    // Find directories less than N bytes

    // Start at the root
    fs.cd("/")

    // Our result
    var sum = 0

    // BFS
    var q: Queue<INode> = LinkedList()
    var visited = HashSet<INode>()

    q.add(fs.head)
    visited.add(fs.head)
    while (!q.isEmpty()) {
        val node = q.remove()

        // Our matching function
        if (node.nodeType == INodeType.DIR && node.totalSize() <= 100_000) {
            println("$node.name matches (size=${node.totalSize()})")
            sum += node.totalSize()
        }

        for (child in node.children) {
            if (!visited.contains(child)) {
                q.add(child)
            }
        }
    }

    println("Final: $sum")
}

package main.kotlin.com.antzucaro.aoc.day7

import java.util.*
import kotlin.collections.HashSet

class Part2 {
}

fun main(args: Array<String>) {
    val fs = load(args[0])

    // Initial calculations
    val maxSpace = 70_000_000
    val toFree = 30_000_000
    val currentFree = maxSpace - fs.root.totalSize()

    // Final result
    var smallest: INode? = null

    // BFS
    var q: Queue<INode> = LinkedList()
    var visited = HashSet<INode>()

    q.add(fs.root)
    visited.add(fs.root)
    while (!q.isEmpty()) {
        val node = q.remove()

        // Our matching function
        if (node.nodeType == INodeType.DIR) {
            if (currentFree + node.totalSize() >= toFree) {
                println("deleting $node would save ${node.totalSize()} space")
                if(smallest == null || smallest.totalSize() > node.totalSize()) {
                    smallest = node
                }
            }
        }

        for (child in node.children) {
            if (!visited.contains(child)) {
                q.add(child)
            }
        }
    }

    print(smallest!!)
}

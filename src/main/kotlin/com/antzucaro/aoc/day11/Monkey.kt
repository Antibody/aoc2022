package main.kotlin.com.antzucaro.aoc.day11

import java.io.File
import kotlin.collections.ArrayList

data class Monkey(
    var id: Int = 0,
    var items: ArrayList<Long> = ArrayList(),
    var operation: (Long)->Long = { e -> e },
    var divBy: Long = 0,
    var modBy: Long = 0,
    var condition: (Long)->Boolean = { e -> true },
    var passIfTrue: Int = 0,
    var passIfFalse: Int = 0,
    var numInspections: Long = 0,
    ) {

    fun inspect(i: Int) {
        println("  Monkey inspects an item with a worry level of ${items[i]}.")

        items[i] = operation(items[i])
        println("    Worry level is changed to ${items[i]}.")

        if (modBy != 0L) {
            // Used to keep worry manageable in part 2
            items[i] = items[i] % modBy
            println("    Monkey gets bored with item. Worry level is modded by ${modBy} to ${items[i]}.")
        } else {
            items[i] = items[i]/3
            println("    Monkey gets bored with item. Worry level is divided by 3 to ${items[i]}.")
        }

        numInspections++
    }

    fun test(i: Int): Int {
        val result = condition(items[i])
        val passTo: Int = if (result) {
            println("    Current worry level passes the test.")
            passIfTrue
        } else {
            println("    Current worry level does not pass the test.")
            passIfFalse
        }
        println("    Item with worry level ${items[i]} is thrown to monkey $passTo.")
        return passTo
    }
}

fun load(filename: String): ArrayList<Monkey> {
    val monkeys = ArrayList<Monkey>()

    var monkey = Monkey()
    File(filename).forEachLine { line ->
        if (line.trimStart().startsWith("Monkey")) {
            monkey.id = Integer.parseInt(line.removePrefix("Monkey ").trimEnd(':'))
        } else if (line.contains("Starting items:")) {
            val (_, itemsStr) = line.split("Starting items: ")
            val items = itemsStr.split(", ").map { e -> Integer.parseInt(e).toLong() }
            monkey.items = ArrayList(items)
        } else if (line.contains("Operation:")) {
            val (_, opStr) = line.split("Operation: new = ")
            val item = opStr.split(" ").last()

            var toApply = 0
            if (item != "old") {
                toApply = Integer.parseInt(item)
            }

            if (opStr.contains("+")) {
                if (item == "old") {
                    monkey.operation = { e -> e + e }
                } else {
                    monkey.operation = { e -> e + toApply }
                }
            } else if (opStr.contains("*")) {
                if (item == "old") {
                    monkey.operation = { e -> e * e }
                } else {
                    monkey.operation = { e -> e * toApply }
                }
            }
        } else if (line.contains("Test")) {
            val (_, divByStr) = line.split("Test: divisible by ")
            val divBy = Integer.parseInt(divByStr).toLong()
            monkey.divBy = divBy
            monkey.condition = { e -> e % divBy == 0L }
        } else if (line.trimStart().startsWith("If")) {
            val throwTo = Integer.parseInt(line.split("throw to monkey ").last())
            if (line.contains("If true")) {
                monkey.passIfTrue = throwTo
            } else {
                monkey.passIfFalse = throwTo
            }
        } else if (line.isBlank()) {
            monkeys.add(monkey)
            monkey = Monkey()
        }
    }
    monkeys.add(monkey)
    return monkeys
}

fun turn(monkey: Monkey, monkeys: ArrayList<Monkey>) {
    while(monkey.items.isNotEmpty()) {
        monkey.inspect(0)
        val passTo = monkey.test(0)
        monkeys[passTo].items.add(monkey.items.removeAt(0))
    }
}

fun round(i: Int, monkeys: ArrayList<Monkey>) {
    println("--- START OF ROUND $i ---")
    for (monkey in monkeys) {
        println("Monkey ${monkey.id}:")
        turn(monkey, monkeys)
    }
    println("--- END OF ROUND $i ---")
}

fun main(args: Array<String>) {
    // ---------------------------
    // Part 1
    // ---------------------------
    val monkeys = load(args[0])

    var numRounds = 20
    for (i in 1..numRounds) {
        round(i, monkeys)
    }

    // ---------------------------
    // Part 2
    // ---------------------------
    val monkeysp2 = load(args[0])

    // Calculate
    var modBy = 1L
    for (monkey in monkeysp2) {
        modBy *= monkey.divBy
    }

    // Provide to all monkeys
    for (monkey in monkeysp2) {
        monkey.modBy = modBy
    }

    // Now we can run!
    numRounds = 10_000
    for (i in 1..numRounds) {
        round(i, monkeysp2)
    }

    val topTwo = monkeysp2.map{ m -> m.numInspections }.sorted().takeLast(2)
    val monkeyBusiness = topTwo[0] * topTwo[1]
    println(monkeyBusiness)
}
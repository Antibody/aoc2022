package main.kotlin.com.antzucaro.aoc.day10

import java.io.File

enum class InstructionType (val delay: Int){
    NOOP(1), ADDX(2),
}

data class Instruction(val instructionType: InstructionType, val value: Int, var cyclesLeft: Int = instructionType.delay)

class CRT {
    private var x = 1
    private var tickNumber: Int = 0
    private var sumSignal = 0

    private fun drawSprite() {
        if ((tickNumber) % 40 == 0) {
            println()
        }

        val left = x - 1
        val right = x + 1
        val col = tickNumber % 40
        if (col in left..right) {
            print("#")
        } else {
            print(".")
        }
    }
    private fun updateSignal() {
        if ((tickNumber == 20 || (tickNumber-20) % 40 == 0) && tickNumber <= 220) {
            sumSignal += signalStrength()
        }
    }

    fun process(instruction: Instruction) {
        drawSprite()
        tickNumber += 1

        updateSignal()
        when(instruction.instructionType) {
            InstructionType.ADDX -> {
                drawSprite()
                tickNumber += 1
                updateSignal()
                x += instruction.value
            }
            else -> {}
        }
    }

    private fun signalStrength(): Int {
        return x * tickNumber
    }

    fun getSumSignal(): Int {
        return sumSignal
    }
}

fun process(file: String) {
    val crt = CRT()

    File(file).forEachLine { line ->
        val instruction: Instruction = if (line.startsWith("addx")) {
            val (_, valueStr) = line.split(" ")
            Instruction(InstructionType.ADDX, Integer.parseInt(valueStr))
        } else {
            Instruction(InstructionType.NOOP, 0)
        }

        crt.process(instruction)
    }
    println("sum of signal strengths for those ticks: ${crt.getSumSignal()}")
}

fun main(args: Array<String>) {
    process(args[0])
}
package main.kotlin.com.antzucaro.aoc.day1

import java.io.File
import java.util.*

data class Elf(val ID: Int = 0, var calories: Int = 0):Comparable<Elf> {
    override fun compareTo(other: Elf): Int {
        return when {
            this.calories > other.calories -> {
                1;
            }
            this.calories < other.calories -> {
                -1;
            }
            else -> {
                0;
            }
        }
    }
}

class ElfCalorieCounter {
    var queue: PriorityQueue<Elf> = PriorityQueue(Collections.reverseOrder());

    fun process(filename: String) {
        println("Processing $filename");

        // Elf number
        var elfNumber = 1;

        // Current sum of the calories for this elf.
        var currentSum = 0;

        File(filename).forEachLine { line ->
            if (line.isBlank()) {
                queue.add(Elf(elfNumber++, currentSum))
                currentSum = 0;
            } else {
                currentSum += Integer.parseInt(line);
            }
        }
        queue.add(Elf(elfNumber++, currentSum))

        println()
        println("Elves with highest calories:")

        var sum = 0
        for(i in 1..3) {
            val elf = queue.poll()
            sum += elf.calories
            println(elf)
        }
        println("Sum of those three elves calories: $sum")
    }
}

fun main(args : Array<String>) {
    val calorieCounter = ElfCalorieCounter()
    calorieCounter.process(args[0])
}
